import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:everything/Clients/clients.dart';
import 'package:everything/Models/models.dart';
import 'package:meta/meta.dart';

import 'news_ui_bloc.dart';

abstract class NewsCallState extends Equatable {
  NewsCallState([List props = const []]) : super(props);
}

class StartN extends NewsCallState {}
class PendingN extends NewsCallState {}

class SuccessN extends NewsCallState {
  final List<News> news;

  SuccessN({@required this.news}) : assert(news != null), super([news]);

}

class FailedN extends NewsCallState {}

class NewsBloc extends Bloc<NewsUiEvent, NewsCallState> {
  final NewsClient newsClient;

  NewsBloc({@required this.newsClient}) : assert(newsClient != null);

  @override
  NewsCallState get initialState => StartN();

  @override
  Stream<NewsCallState> mapEventToState(
      NewsCallState currentState,
      NewsUiEvent event,
      ) async*{
    if (event is FetchNews) {

      yield PendingN();
      try {
        final List<News> news = await newsClient.getNews(event.city);
        yield SuccessN(news: news);
      } catch (_) {
        yield FailedN();
      }

    }
  }

}