import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

abstract class NewsUiEvent extends Equatable {

  NewsUiEvent([List props = const []]) : super(props);
}

class FetchNews extends NewsUiEvent {
  final String city;

  FetchNews({@required this.city})
      : assert(city != null),
        super([city]);
}