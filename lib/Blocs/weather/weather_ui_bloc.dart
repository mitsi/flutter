import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

abstract class WeatherUiEvent extends Equatable {
  WeatherUiEvent([List props = const []]) : super(props);
}

class FetchWeather extends WeatherUiEvent {
  final String city;

  FetchWeather({@required this.city})
      : assert(city != null),
        super([city]);
}