import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:everything/Clients/clients.dart';
import 'package:everything/Models/models.dart';
import 'package:meta/meta.dart';

import 'weather_ui_bloc.dart';

abstract class WeatherCallState extends Equatable {
  WeatherCallState([List props = const []]) : super(props);
}

class Start extends WeatherCallState {}

class Pending extends WeatherCallState {}

class Success extends WeatherCallState {
  final List<Weather> weathers;

  Success({@required this.weathers})
      : assert(weathers != null),
        super([weathers]);
}

class Failure extends WeatherCallState {}

class WeatherBloc extends Bloc<WeatherUiEvent, WeatherCallState> {
  final WeatherClient weatherClient;

  WeatherBloc({@required this.weatherClient}) : assert(weatherClient != null);

  @override
  WeatherCallState get initialState => Start();

  @override
  Stream<WeatherCallState> mapEventToState(
    WeatherCallState currentState,
    WeatherUiEvent event,
  ) async* {
    if (event is FetchWeather) {
      yield Pending();
      try {
        final List<Weather> weathers = await weatherClient.getWeather(event.city);
        yield Success(weathers: weathers);
      } catch (_) {
        yield Failure();
      }
    }
  }
}
