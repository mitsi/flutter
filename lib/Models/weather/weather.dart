import 'package:equatable/equatable.dart';

enum WeatherCondition {
  snow,
  sleet,
  hail,
  thunderstorm,
  heavyRain,
  lightRain,
  showers,
  heavyCloud,
  lightCloud,
  clear,
  unknown
}

class Weather extends Equatable {
  final String condition;
  final String formattedCondition;
  final double minTemp;
  final double temp;
  final double maxTemp;
  final String created;
  final DateTime lastUpdated;
  final String location;
  final DateTime date;
  final String img;

  WeatherCondition weatherCondition() =>
      _mapStringToWeatherCondition(condition);

  Weather(
      {this.condition,
      this.formattedCondition,
      this.minTemp,
      this.temp,
      this.maxTemp,
      this.created,
      this.lastUpdated,
      this.location,
      this.date,
      this.img})
      : super([
          condition,
          formattedCondition,
          minTemp,
          temp,
          maxTemp,
          created,
          lastUpdated,
          location,
          date,
          img
        ]);

  static List<Weather> fromJson(dynamic json) {
    var location = json['location']['name'];
    var todayWeather = json['current'];
    final forecastdayData = json['forecast']['forecastday'] as List;
    var weathers = List<Weather>();

    for (var i = 0; i < forecastdayData.length; i++) {
      final weather = forecastdayData[i];
      weathers.add(Weather(
        condition: weather['day']['condition']['text'],
        formattedCondition: weather['day']['condition']['text'],
        minTemp: weather['day']['mintemp_c'] as double,
        temp: i == 0
            ? todayWeather["temp_c"]
            : weather['day']['avgtemp_c'] as double,
        maxTemp: weather['day']['maxtemp_c'] as double,
        created: DateTime.now().toString(),
        lastUpdated: DateTime.now(),
        location: location,
        date: new DateTime.fromMillisecondsSinceEpoch(weather['date_epoch'] * 1000),
        img: i == 0
            ? "https:${todayWeather["condition"]["icon"]}"
            :"https:${weather["day"]["condition"]["icon"]}"
      ));
    }

    return weathers;
  }

  static WeatherCondition _mapStringToWeatherCondition(String input) {
    WeatherCondition state;
    switch (input) {
      case 'sn':
        state = WeatherCondition.snow;
        break;
      case 'sl':
        state = WeatherCondition.sleet;
        break;
      case 'h':
        state = WeatherCondition.hail;
        break;
      case 't':
        state = WeatherCondition.thunderstorm;
        break;
      case 'hr':
        state = WeatherCondition.heavyRain;
        break;
      case 'lr':
        state = WeatherCondition.lightRain;
        break;
      case 's':
        state = WeatherCondition.showers;
        break;
      case 'hc':
        state = WeatherCondition.heavyCloud;
        break;
      case 'lc':
        state = WeatherCondition.lightCloud;
        break;
      case 'c':
        state = WeatherCondition.clear;
        break;
      default:
        state = WeatherCondition.unknown;
    }
    return state;
  }

  @override
  String toString() {
    return 'Weather{condition: $condition, formattedCondition: $formattedCondition, minTemp: $minTemp, temp: $temp, maxTemp: $maxTemp, created: $created, lastUpdated: $lastUpdated, location: $location}';
  }
}
