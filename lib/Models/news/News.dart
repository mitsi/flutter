import 'package:equatable/equatable.dart';

class News extends Equatable {
  final String title;
  final String description;
  final String date;
  final String img;

  News({this.title, this.description, this.date, this.img})
      : super([title, description, date, img]);

  static List<News> fromJson(dynamic json) {
    final articles = json['articles'] as List;
    var newsList = List<News>();

    for (var i = 0; i < articles.length; i++) {
      final article = articles[i];
      if (articleIsValid(article)) {
        newsList.add(News(
            title: article['title'],
            description: article['description'],
            date: article['publishedAt'],
            img: article['urlToImage']));
      }
    }

    return newsList;
  }

  @override
  String toString() {
    return 'News';
  }

  static bool articleIsValid(article) {
    return article['title'] != null &&
        article['description'] != null &&
        article['publishedAt'] != null &&
        article['urlToImage'] != null;
  }
}
