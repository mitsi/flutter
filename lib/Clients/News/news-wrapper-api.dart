import 'dart:convert';

import 'package:meta/meta.dart';
import 'package:http/http.dart' as http;

import 'package:everything/Models/models.dart';

class NewsApiWrapper {
  final http.Client httpClient;

  NewsApiWrapper({@required this.httpClient}) : assert(httpClient != null);

  Future<List<News>> fetchNews(String city) async {
    final newsResponse = await this.httpClient.get("https://newsapi.org/v2/everything?sources=le-monde&q=$city&apiKey=b48c3ce0529446039250abdbde15fe18");
    if(newsResponse.statusCode != 200){
      throw Exception('error getting News');
    }
    final newsJson = jsonDecode(newsResponse.body);
    return News.fromJson(newsJson);
  }
}