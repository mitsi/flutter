import 'package:meta/meta.dart';

import 'package:everything/Clients/News/news-wrapper-api.dart';
import 'package:everything/Models/models.dart';

class NewsClient {
  final NewsApiWrapper newsApiWrapper;

  NewsClient({@required this.newsApiWrapper}) : assert(newsApiWrapper != null);

  Future<List<News>> getNews(String city) async {
    return await newsApiWrapper.fetchNews(city);
  }
}