import 'package:meta/meta.dart';

import 'package:everything/Clients/weather/weather_wrapper_api.dart';
import 'package:everything/Models/models.dart';

class WeatherClient {
  final WeatherApiWrapper weatherApiWrapper;

  WeatherClient({@required this.weatherApiWrapper})
      : assert(weatherApiWrapper != null);

  Future<List<Weather>> getWeather(String city) async {
    return await weatherApiWrapper.fetchWeather(city);
  }
}
