import 'dart:convert';

import 'package:meta/meta.dart';
import 'package:http/http.dart' as http;

import 'package:everything/Models/models.dart';

class WeatherApiWrapper {
  static const baseUrl = 'https://api.apixu.com/v1';
  final http.Client httpClient;  

  WeatherApiWrapper({@required this.httpClient}) : assert(httpClient != null);

  Future<List<Weather>> fetchWeather(String city) async {
    final weatherUrl = '$baseUrl/forecast.json?key=ac45d368fd3140e086c111125192102&q=$city&days=7';
    final weatherResponse = await this.httpClient.get(weatherUrl);
    if (weatherResponse.statusCode != 200) {
      throw Exception('error getting weather for location');
    }
    final weatherJson = jsonDecode(weatherResponse.body);
    return Weather.fromJson(weatherJson);
  }
}
