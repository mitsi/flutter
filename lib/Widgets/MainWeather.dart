import 'package:everything/Models/models.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:flutter/rendering.dart';

class MainWeather extends StatelessWidget {
  final Weather weather;

  const MainWeather({Key key, this.weather}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return Column(
      children: <Widget>[
        Padding(padding: EdgeInsets.symmetric(vertical: 10), child: Column(children: <Widget>[
          Text('${weather.location}', style: new TextStyle(color: Colors.black, fontSize: 32.0, fontWeight: FontWeight.w300)),
          Text('${weather.formattedCondition}', style: new TextStyle(color: Colors.black, fontSize: 16.0)),
          Text('${weather.temp.toStringAsFixed(0)}°',  style: new TextStyle(color: Colors.black, fontSize: 80, fontWeight: FontWeight.w200)),
        ],),),
        Row(children: <Widget>[
          Expanded(child: Column(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
            Padding(padding: EdgeInsets.only(left: 15),child: Row(children: <Widget>[Text('${DateFormat.EEEE().format(weather.date)} TODAY', style: new TextStyle(fontWeight: FontWeight.w400),)],))
          ]),),
          Expanded(child: Column(mainAxisAlignment: MainAxisAlignment.end, children: <Widget>[
            Padding(padding: EdgeInsets.only(right: 15),child:
            Row(mainAxisAlignment: MainAxisAlignment.end,children: <Widget>[
              Padding(padding: EdgeInsets.only(right: 30), child: Text('${weather.maxTemp.toStringAsFixed(0)}°'),),
              Text("${weather.minTemp.toStringAsFixed(0)}°", style: TextStyle(color: Color.fromARGB(100, 0, 0, 0)),)],))
          ],),)
        ],)
      ],
    );
  }
}
