import 'package:everything/Models/models.dart';
import 'package:flutter/material.dart';
import 'package:flutter_advanced_networkimage/flutter_advanced_networkimage.dart';
import 'package:intl/intl.dart';


class WeatherItem extends StatelessWidget {
  final Weather weather;

  const WeatherItem({Key key, this.weather}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
        child: Padding(
            padding: EdgeInsets.all(8.0),
            child: Column(
              children: <Widget>[
                Text(DateFormat.EEEE().format(weather.date),
                    style: new TextStyle(color: Colors.black, fontSize: 16.0)),
                Padding(
                    padding: EdgeInsets.all(10.0),
                    child: processImage(weather.img),
                ),
                Text(
                    "${weather.minTemp.toStringAsFixed(0)}°  ${weather.maxTemp.toStringAsFixed(0)}°",
                    style: new TextStyle(color: Colors.black)),
              ],
            )));
  }

  Widget processImage(String uri) {
    return Image(
      image: AdvancedNetworkImage(
        uri,
        useDiskCache: true,
      ),
      fit: BoxFit.cover,
    );
  }
}
