import 'dart:io';

import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'package:flutter/services.dart';
import 'package:flutter_beacon/flutter_beacon.dart';

class BeaconScanner extends StatefulWidget {
  final String userId;

  const BeaconScanner({Key key, this.userId}) : super(key: key);

  @override
  _BeaconScanner createState() => _BeaconScanner();
}

class _BeaconScanner extends State<BeaconScanner> {
  StreamSubscription<RangingResult> _streamRanging;
  DatabaseReference _userBeaconsRef;
  final _regionBeacons = <Region, List<Beacon>>{};
  final _beacons = <Beacon>[];

  FirebaseApp firebaseApp;

  var history = Map<String, String>();

  @override
  void initState() {
    super.initState();
    configureFirebase();
    initBeacon();
  }

  initBeacon() async {
    try {
      await flutterBeacon.initializeScanning;
      print('Beacon scanner initialized');
    } on PlatformException catch (e) {
      print(e);
    }

    final regions = <Region>[];

    if (Platform.isIOS) {
      regions.add(
        Region(
            identifier: 'Cubeacon',
            proximityUUID: 'CB10023F-A318-3394-4199-A8730C7C1AEC'),
      );
      regions.add(Region(
          identifier: 'Apple Airlocate',
          proximityUUID: 'E2C56DB5-DFFB-48D2-B060-D0F5A71096E0'));
      regions.add(Region(
          identifier: 'Nexus',
          proximityUUID: '4F8126AC-C60A-4C60-AB07-8BA506134087'));
    } else {
      regions.add(Region(identifier: 'com.beacon'));
    }

    _streamRanging = flutterBeacon.ranging(regions).listen((result) {
      if (result != null && mounted) {
        setState(() {
          _regionBeacons[result.region] = result.beacons;
          _beacons.clear();
          _regionBeacons.values.forEach((list) {
            _beacons.addAll(list);
          });
          _beacons.sort(_compareParameters);
        });
      }
    });
  }

  int _compareParameters(Beacon a, Beacon b) {
    int compare = a.proximityUUID.compareTo(b.proximityUUID);

    if (compare == 0) {
      compare = a.major.compareTo(b.major);
    }

    if (compare == 0) {
      compare = a.minor.compareTo(b.minor);
    }

    return compare;
  }

  @override
  void dispose() {
    if (_streamRanging != null) {
      _streamRanging.cancel();
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Scanner'),
      ),
      body: _beacons == null
          ? Center(child: CircularProgressIndicator())
          : Column(
              children: <Widget>[
                Text("Current :"),
                Container(
                    child: buildCurrentList(),
                    height: MediaQuery.of(context).size.height * 0.4),
                Text("History :"),
                Container(
                    child: buildHistory(),
                    height: MediaQuery.of(context).size.height * 0.4),
              ],
            ),
    );
  }

  void configureFirebase() async {
    firebaseApp = await FirebaseApp.configure(
      name: 'qwackbotBeacons',
      options: Platform.isIOS
          ? const FirebaseOptions(
              googleAppID: '1:240698124582:ios:868799d08cdf8aff',
              gcmSenderID: '240698124582',
              databaseURL: 'https://qwackproject.firebaseio.com/',
            )
          : const FirebaseOptions(
              googleAppID: '1:240698124582:android:868799d08cdf8aff',
              apiKey: 'AIzaSyCzn9WrRxsetoueTK1CdsV3W6sxWh6kUJ8',
              databaseURL: 'https://qwackproject.firebaseio.com/',
            ),
    );
    _userBeaconsRef = FirebaseDatabase.instance
        .reference()
        .child(widget.userId)
        .child("beacons");
    final dataSnapshotHistory = await _userBeaconsRef.once();
    dataSnapshotHistory.value.forEach((key, value) {
      history[key] = "${value["nbSeen"]}";
    });
  }

  Future<void> addBeacon(String name) async {
    var beaconEntry = await _userBeaconsRef.child(name).once();
    if (beaconEntry.value == null) {
      final result = await createEntry(_userBeaconsRef.child(name));
      print(
          "Isvalue added commited: ${result.committed} error: ${result.error?.message}");
      beaconEntry = await _userBeaconsRef.child(name).once();
    }
    var transactionResult =
        await updateEntry(_userBeaconsRef.child(name), beaconEntry);
    print(
        "Isvalue updated commited: ${transactionResult.committed} error: ${transactionResult.error?.message}");
  }

  Future<TransactionResult> updateEntry(
      DatabaseReference dbRef, DataSnapshot beaconEntry) async {
    return await dbRef.runTransaction((MutableData mutableData) async {
      mutableData.value = {
        'nbSeen': beaconEntry.value["nbSeen"] + 1,
        'date': DateTime.now().millisecondsSinceEpoch
      };
      return mutableData;
    });
  }

  Future<TransactionResult> createEntry(DatabaseReference dbRef) async {
    return await dbRef.runTransaction((MutableData mutableData) async {
      mutableData.value = {
        'nbSeen': 0,
        'date': DateTime.now().millisecondsSinceEpoch
      };
      return mutableData;
    });
  }

  ListView buildCurrentList() {
    return ListView(
      children: ListTile.divideTiles(
          context: context,
          tiles: _beacons.map((beacon) {
            addBeacon(beacon.macAddress);
            return ListTile(
              title: Text(beacon.proximityUUID),
              subtitle: new Row(
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Flexible(
                      child: Text(
                          'Major: ${beacon.major}\nMinor: ${beacon.minor}',
                          style: TextStyle(fontSize: 13.0)),
                      flex: 1,
                      fit: FlexFit.tight),
                  Flexible(
                      child: Text(
                          'Distance: ${beacon.accuracy}m\nRSSI: ${beacon.rssi}',
                          style: TextStyle(fontSize: 13.0)),
                      flex: 2,
                      fit: FlexFit.tight)
                ],
              ),
            );
          })).toList(),
    );
  }

  ListView buildHistory() {
    return ListView.builder(
        itemCount: history.length,
        scrollDirection: Axis.vertical,
        itemBuilder: (context, index) {
          final key = history.keys.toList()[index];
          return Card(child:
            Padding(padding: EdgeInsets.all(8), child:
              Row(children: <Widget>[Expanded(child: Text("$key"),), Expanded(child: Text("Seen: ${history[key]}"),)],)
              ,)
            ,);
        });
  }
}
