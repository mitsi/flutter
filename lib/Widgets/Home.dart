import 'package:everything/Blocs/news/news_bloc.dart';
import 'package:everything/Blocs/news/news_ui_bloc.dart';
import 'package:everything/Blocs/weather/weather_bloc.dart';
import 'package:everything/Blocs/weather/weather_ui_bloc.dart';
import 'package:everything/Clients/News/news-client.dart';
import 'package:everything/Clients/News/news-wrapper-api.dart';
import 'package:everything/Models/models.dart';
import 'package:everything/Widgets/BeaconScanner.dart';
import 'package:everything/Widgets/MainWeather.dart';
import 'package:everything/Widgets/WeatherItem.dart';
import 'package:everything/Widgets/NewsItem.dart';
import 'package:everything/services/authentication.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:geocoder/geocoder.dart';
import 'package:location/location.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:firebase_messaging/firebase_messaging.dart';


class Home extends StatefulWidget {
  final WeatherBloc weatherBloc;
  final BaseAuth auth;
  final String userId;
  final VoidCallback onSignedOut;

  const Home(
      {Key key, this.weatherBloc, this.auth, this.userId, this.onSignedOut})
      : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return new _HomeState(weatherBloc);
  }
}

class _HomeState extends State<Home> {
  final FirebaseMessaging firebaseMessaging = FirebaseMessaging();
  var location = new Location();

  WeatherBloc _weatherBloc;
  BuildContext buildContext;

  NewsBloc _newsBloc;

  _HomeState(WeatherBloc weatherBloc) {
    _weatherBloc = weatherBloc;
    _newsBloc = NewsBloc(newsClient: NewsClient(newsApiWrapper: NewsApiWrapper(httpClient: http.Client())));
  }

  @override
  void initState() {
    super.initState();
    firebaseCloudMessaging_Listeners();
    getLocality();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Météo"),
        actions: <Widget>[
          new FlatButton(
              child: new Text('Scanner', style: new TextStyle(fontSize: 17.0)),
              onPressed: (){
                setState(() {
                  Navigator.of(context).push(
                      new MaterialPageRoute(
                          builder: (BuildContext context) => BeaconScanner(userId: widget.userId,)));
                });
              }
          ),
          new FlatButton(
              child: new Text('Logout', style: new TextStyle(fontSize: 17.0)),
              onPressed: _signOut),
        ],
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            Column(children:<Widget>[buildMeteoBuilder()]),
            Column(children:<Widget>[buildNewsBuilder()])
          ],
        ),
      ),
    );
  }

  Widget buildWeatherOfTheDay(Weather todayWeather) {
    return Padding(
        padding: EdgeInsets.all(10), child: MainWeather(weather: todayWeather));
  }

  SafeArea buildWeatherHorizontalViewPagerList(List<Weather> otherWeathers) {
    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Container(
          height: MediaQuery.of(context).size.width * 0.4,
          child: ListView.builder(
              itemCount: 5,
              scrollDirection: Axis.horizontal,
              itemBuilder: (context, index) {
                return WeatherItem(
                  weather: otherWeathers[index],
                );
              }),
        ),
      ),
    );
  }

  Widget buildNewsVertical(List<News> allNews){
    return  ListView.builder(
      itemCount: allNews.length,
      itemBuilder: (context, index) {
        return NewsItem(news: allNews[index]);
      });
  }

  _signOut() async {
    try {
      await widget.auth.signOut();
      widget.onSignedOut();
    } catch (e) {
      print(e);
    }
  }

  void firebaseCloudMessaging_Listeners() {

    firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) {
        print('on message $message');
        dynamic notification = message['notification'];
        print(notification['title']);
        print(notification['body']);
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: new Text(notification['title']),
              content: new Text(notification['body']),
            );
          }
        );
      },
      onResume: (Map<String, dynamic> message) {
        print('on resume $message');
      },
      onLaunch: (Map<String, dynamic> message) {
        print('on launch $message');
      },
    );
    firebaseMessaging.requestNotificationPermissions(
        const IosNotificationSettings(sound: true, alert: true, badge: true));
    firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings setting) {
      print('IOS Setting Registed');
    });
    firebaseMessaging.getToken().then((token) {
      print(token);
    });
  }

  void getLocality([city]) async {
    final locationData = await location.getLocation();
    print(locationData.latitude);
    try{
      var coordinates = Coordinates(locationData.latitude, locationData.longitude);
      var addresses = await Geocoder.local.findAddressesFromCoordinates(coordinates);
      _weatherBloc.dispatch(FetchWeather(city: addresses.first.locality));
      _newsBloc.dispatch(FetchNews(city: addresses.first.locality));
    }on PlatformException {
      snackBar("Error while retreiving position data");
      return;
    }

  }

  void snackBar(String message) {
    Scaffold.of(buildContext).showSnackBar(new SnackBar(
      content: new Text(message),
      duration: new Duration(seconds: 5),
    ));
  }

  Widget buildMeteoBuilder() {
    return BlocBuilder(
      bloc: _weatherBloc,
      builder: (BuildContext context, WeatherCallState weatherCallState) {
        this.buildContext = context;
        if (weatherCallState is Pending || weatherCallState is Start) {
          return SpinKitDoubleBounce( key: Key("loadinSpinKitAnimation"),
              color: Color.fromARGB(100, 150, 150, 150));
        }
        if (weatherCallState is Success) {
          final todayWeather = weatherCallState.weathers[0];
          final otherWeathers = weatherCallState.weathers.sublist(1);
          return Center(
              child: Column(children: <Widget>[
                buildWeatherOfTheDay(todayWeather),
                buildWeatherHorizontalViewPagerList(otherWeathers),
              ]));
        }
        if (weatherCallState is Failure) {
          return Center(child: Text("Okay smtg wrong"));
        }
      },
    );
  }

  Widget buildNewsBuilder() {
    return BlocBuilder(
      bloc: _newsBloc,
      builder: (BuildContext context, NewsCallState newsCallState) {
        this.buildContext = context;
        if (newsCallState is PendingN || newsCallState is StartN) {
          return SpinKitDoubleBounce( key: Key("loadinSpinKitAnimation"),
              color: Color.fromARGB(100, 150, 150, 150));
        }
        if (newsCallState is SuccessN) {
          final actualNews = newsCallState.news;
          return Container(
            height: MediaQuery.of(context).size.height * 0.30,
            child:  buildNewsVertical(actualNews),);
        }
        if (newsCallState is FailedN) {
          return Center(child: Text("Okay smtg wrong"));
        }
      },
    );
  }
}

