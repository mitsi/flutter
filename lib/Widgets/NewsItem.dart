import 'package:everything/Models/models.dart';
import 'package:flutter/material.dart';
import 'package:flutter_advanced_networkimage/flutter_advanced_networkimage.dart';

class NewsItem extends StatelessWidget {
  final News news;

  const NewsItem({Key key, this.news}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Row(
        children: <Widget>[
          Column(
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(left: 2.0),
                height: 110,
                width: 110,
                child: Center(
                  child: processImage(news.img),
                ),
              ),
            ],
          ),
          Padding(
            padding: EdgeInsets.only(left: 2.0),
            child: Column(
              children: <Widget>[
                Container(
                  width: MediaQuery.of(context).size.width * 0.65,
                  child: Text(news.title,
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width * 0.65,
                  child: Text(news.description),
                ),
                Container(
                  width: MediaQuery.of(context).size.width * 0.65,
                  child: Text(news.date,
                    style: TextStyle(color: Colors.grey),
                  ),
                )
              ],
            ),
          )

        ],
      ),
    );
  }

  Widget processImage(String uri) {
    return Image(
      image: AdvancedNetworkImage(
        uri,
        useDiskCache: true,
      ),
      fit: BoxFit.cover,
    );
  }
  
}
