# Flutter

- Utilisation de firebase
- auth firebase
- firebase/firestore
- firebase cloud messaging
- Animation intégrant flare
- Git
- Trello pour la gestion de projet
- codemagic.io
- Soutenance sur un téléphone

# Application

- Sa recupere la météo, la position avec le gps
- On recupere les news (genre google news sur la position)
- On detecte les beacons en bluetooth, on ajoute les données dans firebase
- On peut les afficher sur une map par exemple
- Authent par firebase auth
- On fout une animation flare avec la météo ou juste après la connexion
- Notification push tous les matins pour dire la température avec un commentaire