import 'package:everything/Blocs/blocs.dart';
import 'package:everything/Clients/clients.dart';
import 'package:everything/Widgets/Home.dart';
import 'package:everything/pages/login_signup_page.dart';
import 'package:everything/services/authentication.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:http/http.dart' as http;


class AuthMockClient extends Mock implements BaseAuth {}

void main() {
  testWidgets('Can display homepage', (WidgetTester tester) async {
    Widget homWidget = new MediaQuery(
        data: new MediaQueryData(),
        child: new MaterialApp(
            home: new Home(
              weatherBloc: WeatherBloc(weatherClient:  WeatherClient(weatherApiWrapper: WeatherApiWrapper(httpClient: http.Client()))),
              auth: null,
              userId: null,
              onSignedOut: null,
            )));
    await tester.pumpWidget(homWidget);
    expect(find.text('Météo'), findsOneWidget);
    expect(find.text('Logout'), findsOneWidget);
  });

  testWidgets('Loading animation is displayed', (WidgetTester tester) async {
    Widget homeWidget = new MediaQuery(
        data: new MediaQueryData(),
        child: new MaterialApp(
            home: new Home(
              weatherBloc: WeatherBloc(weatherClient:  WeatherClient(weatherApiWrapper: WeatherApiWrapper(httpClient: http.Client()))),
              auth: null,
              userId: null,
              onSignedOut: null,
            )));
    await tester.pumpWidget(homeWidget);
    expect(find.byKey(Key("loadinSpinKitAnimation")), findsNWidgets(2));
  });
}
