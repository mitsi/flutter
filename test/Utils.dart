import 'package:flutter_test/flutter_test.dart';

class Utils {
  static Future typeIntoTextField(WidgetTester tester, Finder finder, String valueToWrite) async {
  await tester.tap(finder);
  await tester.enterText(finder, valueToWrite);
  await tester.pump(Duration(milliseconds: 400));
  }
}