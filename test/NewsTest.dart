import 'dart:convert';

import 'package:everything/Models/news/News.dart';
import "package:test/test.dart";


void main() {
   final newsJson = jsonDecode('{"status": "ok","totalResults": 10,"articles": [{"source": {"id": "google-news","name": "Google News"},"author": null,"title": "Aftermath: Alabama\'s tornado dead range in age from 6 to 89","description": "President Trump on Tuesday signaled the White House will not comply with a barrage of congressional investigations, accusing Democrats in the House of launching the probes to hurt his chances of winning reelection in 2020.","url": "https://www.foxnews.com/us/aftermath-alabamas-tornado-dead-range-in-age-from-6-to-89","urlToImage": "https://static.foxnews.com/foxnews.com/content/uploads/2019/03/ContentBroker_contentid-7588689f3af542e0b3d1e23bbbcbcc83.png","publishedAt": "2019-03-05T20:44:29+00:00","content": "BEAUREGARD, Ala. The youngest victim was 6, the oldest 89. One extended family lost seven members.The 23 people killed in the nation\'s deadliest tornado in nearly six years came into focus Tuesday with the release of their names by coroner. They included … [+4340 chars]"}]}');

  test("Can parse json from api", () {
    final news = News.fromJson(newsJson);
    expect(news.length, 1);
    var firstNews = news[0];
    expect(firstNews.title, "Aftermath: Alabama\'s tornado dead range in age from 6 to 89");
    expect(firstNews.description, "President Trump on Tuesday signaled the White House will not comply with a barrage of congressional investigations, accusing Democrats in the House of launching the probes to hurt his chances of winning reelection in 2020.");
    expect(firstNews.date, "2019-03-05T20:44:29+00:00");
    expect(firstNews.img, "https://static.foxnews.com/foxnews.com/content/uploads/2019/03/ContentBroker_contentid-7588689f3af542e0b3d1e23bbbcbcc83.png");
  });
}