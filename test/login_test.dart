import 'package:everything/pages/login_signup_page.dart';
import 'package:everything/services/authentication.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

import 'Utils.dart';

class AuthMockClient extends Mock implements BaseAuth {}

void main() {
  testWidgets('Can swicth between sing in and sing up',
      (WidgetTester tester) async {
    // Build our app and trigger a frame.
    Widget signinPage = new MediaQuery(
        data: new MediaQueryData(),
        child: new MaterialApp(
            home: new LoginSignUpPage(
          auth: null,
          onSignedIn: null,
        )));

    await tester.pumpWidget(signinPage);
    expect(find.byIcon(Icons.mail), findsOneWidget);
    expect(find.byIcon(Icons.lock), findsOneWidget);
    await tester.pump();
    await tester.tap(find.text("Create an account"));
    await tester.pump();
    expect(find.text('Create account'), findsOneWidget);
    await tester.tap(find.text("Have an account? Sign in"));
    await tester.pump();
    expect(find.text('Login'), findsOneWidget);
  });

  testWidgets('Cannot connect', (WidgetTester tester) async {
    final AuthMockClient authMockClient = AuthMockClient();
    when(authMockClient.signIn(any, any)).thenThrow(PlatformException(
        code: "exception",
        message:
            "There is no user record corresponding to this identifier. The user may have been deleted.",
        details: null));

    Widget signinPage = new MediaQuery(
        data: new MediaQueryData(),
        child: new MaterialApp(
            home: new LoginSignUpPage(
          auth: authMockClient,
          onSignedIn: () => null,
        )));

    await tester.pumpWidget(signinPage);
    var mailWidgetFinder = find.byKey(Key('email'));
    var passwordWidgetFinder = find.byKey(Key('password'));

    await Utils.typeIntoTextField(tester, mailWidgetFinder, "a@a.com");
    await Utils.typeIntoTextField(tester, passwordWidgetFinder, "test1234");

    await tester.tap(find.text("Login"));
    await tester.pump(Duration(milliseconds: 400));
    expect(
        find.text(
            "There is no user record corresponding to this identifier. The user may have been deleted."),
        findsOneWidget);
    // Clear animation on login page
    await tester.pump(Duration(milliseconds: 500));
  });

  testWidgets('Can connect', (WidgetTester tester) async {
    final AuthMockClient authMockClient = AuthMockClient();
    when(authMockClient.signIn(any, any)).thenAnswer((_) {
      return Future.delayed(const Duration(milliseconds: 500), () => "1");
    });

    Widget signinPage = new MediaQuery(
        data: new MediaQueryData(),
        child: new MaterialApp(
            home: new LoginSignUpPage(
          auth: authMockClient,
          onSignedIn: () => null,
        )));

    await tester.pumpWidget(signinPage);
    var mailWidgetFinder = find.byKey(Key('email'));
    var passwordWidgetFinder = find.byKey(Key('password'));

    await Utils.typeIntoTextField(tester, mailWidgetFinder, "a@a.com");
    await Utils.typeIntoTextField(tester, passwordWidgetFinder, "test1234");

    await tester.tap(find.text("Login"));
    await tester.pump();
    expect(find.byKey(Key("loginProgressIndicator")), findsOneWidget);
    await tester.pump(Duration(milliseconds: 500));
    expect(find.byKey(Key("loginProgressIndicator")), findsNothing);
    expect(
        find.text(
            "There is no user record corresponding to this identifier. The user may have been deleted."),
        findsNothing);
  });

  testWidgets('cannot sign in if fields are empty',
      (WidgetTester tester) async {
    // Build our app and trigger a frame.
    Widget signinPage = new MediaQuery(
        data: new MediaQueryData(),
        child: new MaterialApp(
            home: new LoginSignUpPage(
          auth: null,
          onSignedIn: null,
        )));

    await tester.pumpWidget(signinPage);
    expect(find.byIcon(Icons.mail), findsOneWidget);
    expect(find.byIcon(Icons.lock), findsOneWidget);
    await tester.tap(find.text("Login"));
    await tester.pump(Duration(milliseconds: 200));
    expect(find.text("Email can't be empty"), findsOneWidget);
    expect(find.text("Password can't be empty"), findsOneWidget);
    expect(find.byKey(Key("loginProgressIndicator")), findsNothing);
    // Clear animation on login page
    await tester.pump(Duration(milliseconds: 500));
  });
}